# Orders Search Frontend

# Version history

## Released
### 1.0
Initial version (see Usage sections below).

## Planned
### 1.1
* Configure awesome icons to show magnifier glass
* Replace Order-Details navigation to new page with a Modal window.
* Deploy to a cloud hosting service like render.com
* Externalise backend URL, so the UI can be deployed on different server than the backend.
* Replace ECMAScript by TypeScript.

# Introduction
The UI major functionality can be summarized as showing list of orders filtered by a product.

The UI offers its functionality using RESTFull API consisting of 2 endpoints. The first one delivers the list of available products, and the second one delivers the list of all orders or orders being flitered by a product.

# Usage on Desktop

The UI firt page presents a search (select) box, where the user can select the product name to filter orders by such a product. 

<img title="inital screen" alt="inital screen" src="/UI-images/01-inital_screen.png" width="600">.


<img title="product selection" alt="product selection" src="/UI-images/02-product_selection.png" width="300"> 

The user can also select the product by typing product name (or product id) to filter orders by such a product.

<img title="product selection typing" alt="product selection typing" src="/UI-images/03-product_selection_typing.png" width="300">

All orders filtered by the selected products are shown.

Unshipped orders are shown in ligh blue colour. 

Each matching order, shows the shiping address, city, region, postal code, and country. 

If an order contains 4 or a fewer products, then all products' names are shown. 

<img title="orders found" alt="orders found" src="/UI-images/04-orders_found.png" width="600"> 

Otherwise, 3 products names are shown, in addtion to a number indicating the count of unshown products' names, e.g. + 22 more ... as shown in screenshot below.

<img title="order with more than 4 products" alt="order with more than 4 products" src="/UI-images/06-order_with_4+products.png" width="500">

When the checkbox, under the search field, is checked then only shipped orders are shown.

<img title="orders found filtered" alt="orders found filtered" src="/UI-images/05-orders_found_filtered.png" width="600"> 


# Mobile layout
Mobile usage is the same as on Desctop, but the layout differs to adapt to smaller screen, as shown on screenshot below.

<img title="responsive layout" alt="responsive layout" src="/UI-images/07-mobile-layout.png" width="400"> 

# Design desicion
* To have the application performant in terms of requests made, the application at initial screen, downloads the list of distinct products, to use them for filtering. This also offers better use experience (avoiding typos) and avoid validation errors in response.
* To simplify the implementation, for this assignmenent, viewing all orders is offered with Next and Previous page buttons, instead of pages' numbers.
* Filtering of shipped only orders happens on client side and on the currently displayed 
page only.

# Known issues 
* The UI application expects the backend to be availalbe on same host, where it was downloaded from or where it runs - when both run on localhost - (see Config.js).
* As filtering of orders occurs on the client side, it is possible that the page
shows no orders if all the orders on current page are unshipped.

# Testing
Unit and Integration tests (*TODO*).

# Technology stack
The UI is implemented using:
* `React 18`.
* `ECMAScript 6 (2015)`.
* `Bootstrap 5`.
* `CSS 2`.

## Setup history
The project was generated with [Create React App](https://github.com/facebook/create-react-app).

# CI
This section is on how to further develop (build and deploy) the code.

## Prerequisites
* `NodeJS 18` (with `NPM 9`).
* IDE that support JavaScript (e.g. VSCode version: 1.76 or newer)
* Web browser with development tools (e.g. Edge 111 or newer).

## Available Scripts

After having the prerequisites installed, and checking out the project from Git repository, in the project directory, you can run:

### `npm start`

Downloads the Node modules required, builds the code, then runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### Deployment

#### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\

To deploy the app, copy all content in the `build` folder to the web server, where the end user can navigate to the path pointing to index.html

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

See [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)


#### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't customize it when you are ready for it.

### Advanced Configuration

See [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)
