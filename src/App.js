import './App.css';
import { useRef } from "react";
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';


// Pages
import SearchOrdersByProducts from './pages/SearchOrdersByProducts';
import OrderDetails from './pages/OrderDetails';

function App() {
  // Static storage of product list used by SearchOrdersByProducts
  const staticProductList = useRef(null);

  return (
    <div className="App">
      <header className="App-header">
          NORTHWIND
      </header>
      <div className='App-body'>
        <BrowserRouter>
          <Routes>
            <Route path='/orders' 
              element={<SearchOrdersByProducts productsRef={ staticProductList } />} />
            <Route path='/order/:id' 
              element={<OrderDetails />} />
            <Route path='*' element={<Navigate to={'/orders'} />}/>
          </Routes>
        </BrowserRouter>
      </div>
    </div>
  );
}

export default App;
