const port = 8080;
const baseURL = 'http://' + window.location.hostname + ':' + port;

const Config = { baseURL };
export default Config;