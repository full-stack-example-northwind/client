import { useParams } from "react-router-dom";

export default function OrderDetails() {
  const { id } = useParams();
  return (
    <div className="orderDetails">
      <h3>Order #{id}</h3>
      <p>The assignment does NOT require this page to be implemented!</p>
    </div>
  );
};