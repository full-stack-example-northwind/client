import Config from '../Config';
import { useState, useEffect, useRef } from "react";

import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import Select from 'react-select';
import { useNavigate } from 'react-router-dom';

// productsRef - instance of useRef
export default function SearchOrdersByProducts({ productsRef }) {
  // To get more persistent list of products, fetched list 
  // is stored in ref on the application level.
  const [productList, setProductList] = useState();

  const [pagination, setPagination] = useState();
  const [orders, setOrders] = useState([]);
  const [query, setQuery] = useState(); // { query, start, limit }
  const [selectedProduct, setSelectedProduct] = useState();
  const [shippedOnly, setShippedOnly] = useState(false);
  const pageSize = useRef(20); // TODO user should be able to select this

  // Products should be fetched only once.
  useEffect(() => {
    const firstItem = { ProductID: '*', ProductName: '   --- All ---'};
    if (productsRef.current) { // Product list loaded or loading
      if (productsRef.current.length > 0) {
        // List of products have been loaded, so we can set up productList with its data.
        setProductList([firstItem, ...productsRef.current]);
      }
    } else { // Product list not loaded nor loading
      productsRef.current = []; // Prevent repeated reloads 
      var url = Config.baseURL + '/products/';
      fetch(url)
        .then((res) => res.json())
        .then((jsonData) => {
          productsRef.current = jsonData;
          setProductList([firstItem, ...jsonData]);
        }).catch((err) => {
          productsRef.current = null; // Enable retry
          alert(err);
          console.log(err);
        });
    } 
  }, [productsRef]);

  // Fetch new list of orders when query changes.
  useEffect(() => {
    // Let's not load orders when the component is first rendered.
    if (!query)
      return;
    var url = Config.baseURL + '/orders' + query.query + '&start=' + query.start + '&limit=' + query.limit;
    fetch(url)
      .then((res) => {
        return res.json();
      })
      .then((jsonData) => {
        pageSize.current = jsonData.pagination.limit; // Conform to page size given by server
        setPagination(jsonData.pagination);
        setOrders(jsonData.orders);
      }).catch((err) => {
        alert(err);
        console.log(err);
      })
  }, [query]);

  const handleSelectionChange = (selected) => {
    const selectedId = selected?.ProductID;
    // This can be first time product is selected, so pagination.limit might not be available. 
    // Need to use pageSize.current instead.
    if (selectedId === '*') {
      setQuery({ query: '?', start: 0, limit: pageSize.current });
    } else {
      setQuery({ query: '?ProductID=' + selectedId, start: 0, limit: pageSize.current });
    }
    setSelectedProduct(selected);
  }

  const handleNextPage = () => {
    let newStart = pagination.start + pagination.limit;
    if (newStart >= pagination.totalCount)
      newStart = 0;
    setQuery( { ...query, start: newStart });
  }

  const handlePreviousPage = () => {
    let newStart = pagination.start - pagination.limit;
    if (newStart < 0) { // Take last page
      const total = pagination.totalCount;
      const pageSize = pagination.limit;
      const remainder = (total % pageSize);
      newStart = total - ((remainder > 0) ? remainder : pageSize);
    }
    setQuery( { ...query, start: newStart });
  }

  return (
    <>
      <div className='searchbar'>
        <SearchBar
          productList={productList}
          selectedValue={selectedProduct}
          shippedOnly={shippedOnly}
          onSelectionChange={handleSelectionChange}
          onShippedOnlyChange={setShippedOnly} 
          placeholder='Enter * to get all the orders'
        />
      </div>
      <OrderTable 
        orders={orders}
        shippedOnly={shippedOnly} 
        pagination={pagination}
        onNextPage={handleNextPage}
        onPreviousPage={handlePreviousPage}
      />
    </>
  );
}

function SearchBar({ productList, selectedValue, shippedOnly, 
  onSelectionChange, onShippedOnlyChange, placeholder }) {

  return (
    // For more information, see https://react-select.com/styles
    <>
      <label>Filter orders by product name
      <Select className='searchbox'
        options={productList}
        getOptionValue={(product) => product.ProductID}
        getOptionLabel={(product) => product.ProductName}
        placeholder={placeholder}
        value={selectedValue}
        onChange={onSelectionChange}
        styles={{
          control: (baseStyles, state) => ({
            ...baseStyles,
            borderRadius: '20px',
            fontWeight: 'bold',
            fontSize: '1.2em',
            background: 'rgba(255,255,255,0.08)',
            border: 'none',
            padding: '0.4em',
          }),
          option: (baseStyles, state) => ({
            ...baseStyles,
            color: 'black',
          }),
          input: (baseStyles, state) => ({
            ...baseStyles,
            color: '#FFFFFF',
          }),
          singleValue: (baseStyles, state) => ({
            ...baseStyles,
            color: '#FFFFFF',
          }),
          placeholder: (baseStyles, state) => ({
            ...baseStyles,
            color: '#FFFFFF',
            opacity: 0.5,
          }),
        }}
      />
      </label>
      <Form.Check type='checkbox' label='Show only shipped orders' 
        onChange={(e) => onShippedOnlyChange(e.target.checked)}/>
    </>
  );
}

function OrderTable({ orders, shippedOnly, pagination, onNextPage, onPreviousPage }) {
  const rows = [];

  orders.forEach((order) => {
    if (shippedOnly && !order.ShippedDate)
      return;
    rows.push( <OrderRow order={order} key={order.OrderID}/> );
  }); // TODO try hiding with CSS

  return (
    <>
      <div className='pagination'>
        {pagination && 
          <div className='rowcount'>{rows.length} / {pagination.totalCount} orders shown</div>}
        {pagination && pagination.totalCount > pagination.limit && 
          <>
            <Button onClick={onPreviousPage}>Previous page</Button>
            <span className="pagecount">{pagination.start + 1} - {pagination.start + pagination.limit}</span>
            <Button onClick={onNextPage}>Next page</Button>
          </>
        }
      </div>
      <Container className='productTable'>
        {rows}
      </Container>
    </>
  );
}

function OrderRow({ order }) {
  const navigate = useNavigate();
  const productNames = [];
  if (order.Products) {
    let upTo = order.Products.length > 4 ? 3 : order.Products.length;
    for (let i = 0; i < upTo; ++i)
      productNames.push(order.Products[i].ProductName);
    if (order.Products.length > 4)
      productNames.push('+ ' + (order.Products.length - 3) + ' more...');
  }

  const unshippeColor = { color: order.ShippedDate?null:'cadetblue' };
  return (
    <>
      <span className='idField' style={unshippeColor}>#{order.OrderID}</span>
      <Button className='detailsButton'
        onClick={() => navigate('/order/' + order.OrderID)}>View Details</Button>
      <InfoBox
        title='Shipping address'
        lines={[
          order.ShipAddress, 
          order.ShipCity,
          order.ShipRegion, 
          order.ShipPostalCode,
          order.ShipCountry
        ]} />
      <InfoBox title='Customer name' lines={[order.ContactName]} />
      <InfoBox title='Products' lines={productNames} />
    </>
  );
}

function InfoBox({ title, lines }) {
  const rows = [];
  var i = 0;
  lines.forEach((line) => {
    rows.push(<li key={i++}>{line}</li>);
  });
  
  return (
    <Card className='infoCard'>
      <Card.Title>
        {title}
      </Card.Title>
      <Card.Text>
        {rows}
      </Card.Text>
      </Card>
  );
}